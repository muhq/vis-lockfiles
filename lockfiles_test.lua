#!/usr/bin/env lua5.4

local lockfiles = dofile('lockfiles.lua')

local lunatest = require('lunatest')
function test_percentage_escape() -- luacheck: ignore 111
  local lock_file_path_with_percent = lockfiles.lock_file_path('.', 'foo%bar')
  local lock_file_path_with_slash = lockfiles.lock_file_path('.', 'foo/bar')
  lunatest.assert_not_equal(lock_file_path_with_percent, lock_file_path_with_slash)
end

lunatest.run()
